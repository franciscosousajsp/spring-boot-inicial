package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Estudo;
import com.example.demo.repository.EstudoRepository;

@RestController
@RequestMapping(value = "/api")
public class EstudoController {

	@Autowired
	EstudoRepository estudoRepository;

	@GetMapping("/estudos")
	public List<Estudo> lista() {
		return estudoRepository.findAll();
	}

	@GetMapping("/estudo/{id}")
	public Estudo findById(@PathVariable Long id) {
		return estudoRepository.findById(id).get();
	}
	
	@PostMapping("/estudo")
	public Estudo salvarEstudo(@RequestBody Estudo estudo) {
		return estudoRepository.save(estudo);
	}
	
	@PutMapping("/estudo")
	public Estudo atualizarEstudo(@RequestBody Estudo estudo) {
		return estudoRepository.save(estudo);
	}
	
	@DeleteMapping("/estudo/{id}")
	public void apagarEstudo(@PathVariable Long id) {
		
		estudoRepository.deleteById(id);
	}

}
